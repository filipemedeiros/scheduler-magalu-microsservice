.. |Gitlab License| image:: https://img.shields.io/gitlab/v/license/filipemedeiros/scheduler-magalu-microsservice?style=flat-square
.. |Gitlab Release| image:: https://img.shields.io/gitlab/v/release/filipemedeiros/scheduler-magalu-microsservice?include_prereleases&style=flat-square


|Gitlab License|
|Gitlab Release|


=================
Sumário
=================

- `Introdução`_
- `Dependências`_
- `Configuração`_
- `Como rodar`_
- `Documentação`_
- `Testes`_

============
Introdução
============

O projeto é um desafio de seleção proposto pela Magalu, onde o objetivo é iniciar o desenvolvimento da primeira sprint de uma plataforma de comunicação. O proposto é a criação de 3 endpoints relativos ao agendamento do envio de uma comunicação.

O código foi desenvolvido utilizando Java 17 com banco relacional postgres 14 (bullseye). E também foi utilizado o framework Spring Boot, Lombok, Mockito, Junit5, Swagger e JPA.
    
A API seguiu o padrão RESTful e teve teste unitário com 100% de covarage na camada de serviço.

Também foi gerado um script bash `/run/run.sh` para automatizar o processo de build e iniciar os containers do projeto.


============
Dependências
============

É necessário que você tenha instalado as seguintes dependências:

1. docker
   
2. docker-compose
   
3. git
   
4. jdk17-openjdk


============
Configuração
============

Para configurar o projeto, é necessário que você crie um arquivo `.env` na raiz do projeto.
O arquivo deve conter as seguintes variáveis:

====================    =========================================================
DATASOURCE_USER         ex: scheduler-root
DATASOURCE_PASSWORD     ex: r6!n!RuX@Bm7GKZNb9YDv@UxxtuYX4hx
PORT                    ex: 8080
PORT_DB                 ex: 5436
CONTEXT_PATH            ex: /api
====================    =========================================================

Exemplo de um .env descrito no example.env:

.. code:: bash

    DATASOURCE_USER=scheduler-root
    DATASOURCE_PASSWORD=r6!n!RuX@Bm7GKZNb9YDv@UxxtuYX4hx
    PORT=8080
    PORT_DB=5436
    POSTGRES_DB=scheduler-api
    CONTEXT_PATH=/api

============
Como rodar
============
Para rodar o projeto, execute o comando na pasta raiz do projeto:

.. code:: bash

    chmod +x ./run/run.sh

E posteriormente execute o comando:

.. code:: bash

    ./run/run.sh


============
Documentação
============

A documentação do projeto foi gerada utilizando o _Swagger.
pode ser acessada através do endpoint `/v1/docs`.

.. _Swagger: https://swagger.io/

Os endpoints criados foram:

=============================   =========================================================
**GET** /v1/schedule/{id}       Lista todos os agendamentos
**DELETE** /v1/schedule/{id}    Deleta um agendamento
**POST** /v1/schedule/          Cria um agendamento de comunicação
=============================   =========================================================

Para efeitos de teste, existem os seguintes agendamentos:

**lembrando:** Estes *UUIDs* não foram gerados de forma aleatória, eles são apenas para teste.

=====================================   =========================================================
**UUID**                                **Status**
00000000-0000-0000-0000-000000000001    SCHEDULED
00000000-0000-0000-0000-000000000002    SENT
00000000-0000-0000-0000-000000000003    CANCELED
00000000-0000-0000-0000-000000000004    SCHEDULED
00000000-0000-0000-0000-000000000005    SENT
00000000-0000-0000-0000-000000000006    CANCELED
00000000-0000-0000-0000-000000000007    SCHEDULED
00000000-0000-0000-0000-000000000008    SENT
=====================================   =========================================================


============
Testes
============
Os testes são executados dentro do script de run.sh, mas podem ser executados manualmente.
para isto, execute o comando:

.. code:: bash

    ./mvnw test

