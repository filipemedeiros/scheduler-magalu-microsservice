package com.filipegabriel.scheduler.service;

import com.filipegabriel.scheduler.exception.APIException;
import com.filipegabriel.scheduler.exception.NotFoundException;
import com.filipegabriel.scheduler.model.CommunicationType;
import com.filipegabriel.scheduler.model.Scheduled;
import com.filipegabriel.scheduler.model.Status;
import com.filipegabriel.scheduler.repository.ScheduledRepository;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ScheduleServiceImplTest {

    private final ZonedDateTime DATE_NOW = ZonedDateTime.now();

    @Mock
    private ScheduledRepository scheduledRepository;

    private SchedulingService schedulingService;

    private Scheduled scheduleCreation;
    private Scheduled scheduleCreated;

    private Scheduled scheduleCanceled;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        schedulingService = new SchedulingServiceImpl(scheduledRepository);

        scheduleCreation = Scheduled.builder()
                .senderId(UUID.fromString("00000000-0000-0000-0000-000000000000"))
                .deliveryDate(DATE_NOW.plusDays(1))
                .message("Magalu is the best")
                .communicationType(CommunicationType.WHATSAPP)
                .recipientId(UUID.fromString("00000000-0000-0000-0000-000000000001"))
                .build();

        scheduleCreated = Scheduled.builder()
                .id(UUID.fromString("01234567-8910-1112-1314-151617181920"))
                .createdAt(DATE_NOW)
                .senderId(UUID.fromString("00000000-0000-0000-0000-000000000000"))
                .deliveryDate(DATE_NOW.plusDays(1))
                .message("Magalu is the best")
                .status(Status.SCHEDULED)
                .communicationType(CommunicationType.WHATSAPP)
                .recipientId(UUID.fromString("00000000-0000-0000-0000-000000000001"))
                .build();

        scheduleCanceled = Scheduled.builder()
                .id(UUID.fromString("01234567-8910-1112-1314-151617181920"))
                .createdAt(DATE_NOW)
                .updatedAt(DATE_NOW)
                .senderId(UUID.fromString("00000000-0000-0000-0000-000000000000"))
                .deliveryDate(ZonedDateTime.now().plusDays(1))
                .message("Magalu is the best")
                .status(Status.CANCELED)
                .communicationType(CommunicationType.WHATSAPP)
                .recipientId(UUID.fromString("00000000-0000-0000-0000-000000000001"))
                .build();

    }

    @SneakyThrows
    @Test
    void scheduleSucess() {

        when(scheduledRepository.save(any(Scheduled.class))).thenReturn(scheduleCreated);

        Scheduled result = schedulingService.schedule(scheduleCreation);

        Assertions.assertEquals(scheduleCreated, result);

    }

    @Test
    void scheduleWithMessageNull() {
        scheduleCreation.setMessage(null);
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }

    @Test
    void scheduleWithNullBody() {
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(null));
    }


    @Test
    void scheduleWithMessageEmpty() {
        scheduleCreation.setMessage("");
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }

    @Test
    void scheduleWithSenderIdNull() {
        scheduleCreation.setSenderId(null);
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }

    @Test
    void scheduleWithRecipientIdNull() {
        scheduleCreation.setRecipientId(null);
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }

    @Test
    void scheduleWithCommunicationTypeNull() {
        scheduleCreation.setCommunicationType(null);
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }

    @Test
    void scheduleWithDeliveryDateNull() {
        scheduleCreation.setDeliveryDate(null);
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }

    @Test
    void scheduleWithDeliveryDateInThePast() {
        scheduleCreation.setDeliveryDate(ZonedDateTime.now().minusDays(1));
        Assertions.assertThrows(APIException.class, () -> schedulingService.schedule(scheduleCreation));
    }


    @Test
    void getSchedulingNotFound() {
        when(scheduledRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        Assertions.assertThrows(NotFoundException.class, () -> schedulingService.getScheduling(UUID.fromString("01234567-8910-1112-1314-151617181920")));
    }

    @Test
    void getSchedulingWithIdNull() {
        Assertions.assertThrows(NotFoundException.class, () -> schedulingService.getScheduling(null));
    }

    @Test
    void getSchedulingWithIdEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> schedulingService.getScheduling(UUID.fromString("")));
    }

    @Test
    void getSchedulingWithIdInvalid() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> schedulingService.getScheduling(UUID.fromString("invalid")));
    }

    @Test
    void getSchedulingWithIdNullOrEmpty() {
        Assertions.assertThrows(NotFoundException.class, () -> schedulingService.getScheduling(null));
    }


    @Test
    @SneakyThrows
    void cancelSchedule() {
        when(scheduledRepository.findByIdAndStatusIs(any(UUID.class), eq(Status.SCHEDULED))).thenReturn(Optional.of(scheduleCanceled));
        Scheduled result = schedulingService.cancel(UUID.fromString("01234567-8910-1112-1314-151617181920"));

        Assertions.assertEquals(scheduleCanceled, result);

    }

    @Test
    void cancelScheduleWithIdNull() {
        Assertions.assertThrows(NotFoundException.class, () -> schedulingService.cancel(null));
    }

    @Test
    void cancelScheduleWithIdEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> schedulingService.cancel(UUID.fromString("")));
    }

    @Test
    void cancelScheduleWithIdInvalid() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> schedulingService.cancel(UUID.fromString("invalid")));
    }


    @Test
    void cancelScheduleWithIdNotFound() {
        when(scheduledRepository.findByIdAndStatusIs(any(UUID.class), eq(Status.SCHEDULED))).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> schedulingService.cancel(UUID.fromString("01234567-8910-1112-1314-151617181920")));
    }

}