CREATE TABLE scheduled
(
    id                 UUID        NOT NULL PRIMARY KEY,
    created_at         TIMESTAMP WITH TIME ZONE,
    updated_at         TIMESTAMP WITH TIME ZONE,
    deleted_at         TIMESTAMP WITH TIME ZONE,

    sender_id          UUID        NOT NULL,
    delivery_date      TIMESTAMP WITH TIME ZONE,
    message            TEXT        NOT NULL,
    communication_type VARCHAR(50) NOT NULL,
    status             VARCHAR(50),
    recipient_id       UUID        NOT NULL

);
