package com.filipegabriel.scheduler.repository;

import com.filipegabriel.scheduler.model.Scheduled;
import com.filipegabriel.scheduler.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ScheduledRepository extends JpaRepository<Scheduled, UUID> {

    Optional<Scheduled> findByIdAndStatusIs(UUID id, Status status);
}
