package com.filipegabriel.scheduler.model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Getter
@RequiredArgsConstructor
@Setter
public class Scheduled extends BaseModel {

    @Column(name = "sender_id", nullable = false)
    private UUID senderId;

    @Column(name = "delivery_date", nullable = false)
    private ZonedDateTime deliveryDate;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "communication_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private CommunicationType communicationType;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "recipient_id", nullable = false)
    private UUID recipientId;

    @Builder
    public Scheduled(UUID id, ZonedDateTime createdAt, ZonedDateTime updatedAt, ZonedDateTime deletedAt, UUID senderId, ZonedDateTime deliveryDate, String message, CommunicationType communicationType, Status status, UUID recipientId) {
        super(id, createdAt, updatedAt, deletedAt);
        this.senderId = senderId;
        this.deliveryDate = deliveryDate;
        this.message = message;
        this.communicationType = communicationType;
        this.status = status;
        this.recipientId = recipientId;
    }
}
