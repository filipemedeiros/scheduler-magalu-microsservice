package com.filipegabriel.scheduler.model;

public enum CommunicationType {
    EMAIL,
    SMS,
    PUSH,
    WHATSAPP
}
