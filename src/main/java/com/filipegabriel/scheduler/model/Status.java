package com.filipegabriel.scheduler.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Status {
    SCHEDULED("communication is scheduled for the day "),
    SENT("communication was sent in the day "),
    CANCELED("communication was cancelled in the day ");

    @Getter
    private final String message;
}
