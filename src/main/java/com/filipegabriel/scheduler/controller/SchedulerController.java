package com.filipegabriel.scheduler.controller;


import com.filipegabriel.scheduler.exception.APIException;
import com.filipegabriel.scheduler.exception.NotFoundException;
import com.filipegabriel.scheduler.model.CommunicationType;
import com.filipegabriel.scheduler.model.Scheduled;
import com.filipegabriel.scheduler.model.Status;
import com.filipegabriel.scheduler.service.SchedulingService;
import com.filipegabriel.scheduler.util.DateUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/v1/scheduler")
@AllArgsConstructor
@Tag(name = "Scheduler", description = "Scheduler Controller")
public class SchedulerController {

    private final SchedulingService schedulingService;

    record RequestSchedule(UUID senderId, ZonedDateTime deliveryDate, String message,
                           CommunicationType communicationType, UUID recipientId) {
    }

    @PostMapping(produces = {"application/json"}, consumes = {"application/json"})
    @Operation(summary = "Communication Scheduler", description = "Schedule the sending of the communication")
    public ResponseEntity<?> schedule(@RequestBody RequestSchedule req) {

        ResponseEntity<?> response;

        Scheduled scheduleBody = Scheduled.builder()
                .senderId(req.senderId)
                .deliveryDate(req.deliveryDate)
                .message(req.message)
                .communicationType(req.communicationType)
                .recipientId(req.recipientId)
                .build();

        record StatusResponse(String message, UUID createdId) {
        }

        try {
            Scheduled scheduled = schedulingService.schedule(scheduleBody);
            response = ResponseEntity.status(HttpStatus.CREATED).body(new StatusResponse(Status.SCHEDULED.getMessage() + DateUtil.prettierDate(scheduled.getDeliveryDate()), scheduled.getId()));

        } catch (APIException e) {
            response = ResponseEntity.badRequest().body(e.getMessage());
        }
        return response;

    }

    @GetMapping(value = "/{id}", produces = {"application/json"})
    @Operation(summary = "Find Scheduled Communication by ID", description = "For valid response use UUID type. Other values will generated exceptions")
    public ResponseEntity<?> getScheduling(@PathVariable UUID id) {
        ResponseEntity<?> response;
        try {
            Scheduled scheduled = schedulingService.getScheduling(id);

            record SchedulingResponse(UUID senderId, String deliveryDate, String message,
                                      CommunicationType communicationType,
                                      UUID recipientId, Status status) {
            }

            response = ResponseEntity.ok(new SchedulingResponse(scheduled.getSenderId(), DateUtil.prettierDate(scheduled.getDeliveryDate()), scheduled.getMessage(), scheduled.getCommunicationType(), scheduled.getRecipientId(), scheduled.getStatus()));

        } catch (NotFoundException e) {
            response = ResponseEntity.badRequest().body(e.getMessage());
        }
        return response;
    }

    @DeleteMapping(value = "/{id}", produces = {"application/json"})
    @Operation(summary = "Cancel Scheduled Communication", description = "Cancel the scheduled communication")
    public ResponseEntity<?> cancel(@PathVariable UUID id) {
        ResponseEntity<?> response;

        record CancelResponse(String message) {
        }

        try {
            Scheduled scheduled = schedulingService.cancel(id);
            response = ResponseEntity.ok(new CancelResponse(scheduled.getStatus().getMessage() + DateUtil.prettierDate(ZonedDateTime.now())));
        } catch (NotFoundException e) {
            response = ResponseEntity.badRequest().body(e.getMessage());
        }
        return response;
    }


}
