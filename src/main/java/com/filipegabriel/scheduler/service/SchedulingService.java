package com.filipegabriel.scheduler.service;

import com.filipegabriel.scheduler.exception.APIException;
import com.filipegabriel.scheduler.exception.NotFoundException;
import com.filipegabriel.scheduler.model.Scheduled;

import java.util.UUID;

public interface SchedulingService {

    Scheduled schedule(Scheduled scheduleBody) throws APIException;

    Scheduled getScheduling(UUID scheduledId) throws NotFoundException;

    Scheduled cancel(UUID scheduledId) throws NotFoundException;


}