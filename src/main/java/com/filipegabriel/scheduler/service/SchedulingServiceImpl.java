package com.filipegabriel.scheduler.service;

import com.filipegabriel.scheduler.exception.APIException;
import com.filipegabriel.scheduler.exception.NotFoundException;
import com.filipegabriel.scheduler.model.Scheduled;
import com.filipegabriel.scheduler.model.Status;
import com.filipegabriel.scheduler.repository.ScheduledRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class SchedulingServiceImpl implements SchedulingService {

    private final ScheduledRepository scheduledRepository;


    @Override
    public Scheduled schedule(Scheduled body) throws APIException {
        validate(body);

        body.setStatus(Status.SCHEDULED);
        return scheduledRepository.save(body);

    }

    private void validate(Scheduled body) throws APIException {
        if (Objects.isNull(body))
            throw new APIException("Body is null.");

        if (Objects.isNull(body.getSenderId())) {
            throw new APIException("Sender Id is obligatory. Please, provide a sender Id.");
        }

        if (Objects.isNull(body.getDeliveryDate())) {
            throw new APIException("Delivery Date is obligatory. Please, provide a delivery date.");
        } else if (body.getDeliveryDate().isBefore(ZonedDateTime.now())) {
            throw new APIException("Delivery Date is before now. Please, provide a delivery date after now.");
        }


        if (!StringUtils.isNotBlank(body.getMessage()))
            throw new APIException("Message is obligatory. Please, provide a delivery date.");


        if (Objects.isNull(body.getCommunicationType()))
            throw new APIException("The type of communication is obligatory. Please provide a type of communication.");

        if (Objects.isNull(body.getRecipientId()))
            throw new APIException("Recipient is null.");
    }


    @Override
    public Scheduled getScheduling(UUID scheduledId) throws NotFoundException {
        return scheduledRepository.findById(scheduledId).orElseThrow(() -> new NotFoundException("Scheduled Id: " + scheduledId + " Not Found."));
    }

    @Override
    public Scheduled cancel(UUID scheduledId) throws NotFoundException {
        Scheduled scheduled = this.findScheduled(scheduledId);
        scheduled.setStatus(Status.CANCELED);
        scheduled.setUpdatedAt(ZonedDateTime.now());
        scheduledRepository.save(scheduled);

        return scheduled;
    }


    private Scheduled findScheduled(UUID id) throws NotFoundException {
        return scheduledRepository.findByIdAndStatusIs(id, Status.SCHEDULED).orElseThrow(() -> new NotFoundException("Scheduled is already canceled, sented or does not exist."));
    }


}
