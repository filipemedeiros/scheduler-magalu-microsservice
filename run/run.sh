#!/bin/bash

set -e

printf "\n"
printf '===============================================\n'
printf '========== INICIADO DESAFIO MAGALU ============\n'
printf '===============================================\n'
printf "\n"

echo "             @&&%&%&
            &%%/,*,@
            #((/*,*/
   //(#     *@(,,/&
   (((%      /(*,,,*
  /(((((%%%%&**,***#####
   @@@@@@ &%%#*,/%####&##
         ,&%%%,,,###  &%%#
         &%%%%,,.%###   %%#
        (%&%%%,,.*%###. &%%#
        &&&*,,,,..,..%%  &%#
        &&&&%&%&&%%%%%%   ,/*"



ENV_FILE=.env
if [[ -f "$ENV_FILE" ]]; then
  if [[ ! -s "$ENV_FILE" ]]; then
    echo "O arquivo $ENV_FILE está vazio."
    echo "Por favor preencha e rode novamente o script run/run.sh."
    exit 1
  fi
else
  echo "O arquivo $ENV_FILE não existe."
  echo  "Por favor crie um .env seguindo a documentação."
  printf "\n"
  printf '===============================================\n'
  printf '=============== API NÃO SUBIU =================\n'
  printf '===============================================\n'
  printf "\n"
  exit 1
fi

for dependency in docker docker-compose; do
    if ! command -v "${dependency}" &>/dev/null; then
        not_available+=("${dependency}")
    fi
done

if (( ${#not_available[@]} > 0 )); then
    echo "É necessário que você possua docker e docker-compose, com seu daemon habilitado e que seu usúario tenha permissão." 1>&2
    printf "\n"
    printf '===============================================\n'
    printf '=============== API NÃO SUBIU =================\n'
    printf '===============================================\n'
    printf "\n"
    exit 1
fi

printf "\n"
printf '===============================================\n'
printf '============== INICIANDO TESTES ===============\n'
printf '===============================================\n'
printf "\n"
./run/mvnw test

printf "\n"
printf '===============================================\n'
printf '=========== BUILDANDO A APLICACAO =============\n'
printf '===============================================\n'
printf "\n"
./run/mvnw clean install -Dmaven.test.skip=true


printf "\n"
printf '===============================================\n'
printf '============ INICIANDO OS CONTAINER ===========\n'
printf '===============================================\n'
printf "\n"
docker-compose up -d



printf "\n"
printf '===============================================\n'
printf '============== APLICAÇÃO INICIADA =============\n'
printf '===============================================\n'
printf "\n"

echo "Utilize: \`docker logs scheduler-java\` para verificar os últimos logs."
echo "a Api está disponível de forma local no sufixo: /v1/"
echo "Você pode verificar a documentação em /v1/docs"
printf "\n"
echo "Endpoints disponiveis:"
printf "\n"
echo "POST   /v1/scheduler"
echo "GET    /v1/scheduler/{id}"
echo "DELETE /v1/scheduler{id}"

printf '===============================================\n'
printf '================  FIM DO SCRIPT ===============\n'
printf '===============================================\n'
printf "\n"
